package com.test.mp.mp1111;

import com.test.mp.mp1111.mapper.SpringContextUtil;
import com.test.mp.mp1111.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@SpringBootTest
@RunWith(SpringRunner.class)
public class Mp1111ApplicationTests {

    @Resource
    private UserMapper userMapper;

    @Test
    public void testUpdateById() {
        for (String beanDefinitionName : SpringContextUtil.getApplicationContext().getBeanDefinitionNames()) {
            System.out.println("beanDefinitionName = " + beanDefinitionName);
        }

        if (userMapper == null) {
            System.out.println("空指针");
        }
    }
}
