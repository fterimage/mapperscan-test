package com.test.mp.mp1111;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@MapperScan("com.test.mp.mp1111.mapper")
public class MapperscanApplication {

    public static void main(String[] args) {
        SpringApplication.run(MapperscanApplication.class, args);
    }

}
