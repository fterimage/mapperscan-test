package com.test.mp.mp1111.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author miemie
 * @since 2018-08-10
 */
@Configuration
@MapperScan("com.test.mp.mp1111.mapper")
public class MybatisPlusConfig {

}
