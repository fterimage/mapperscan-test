package com.test.mp.mp1111.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data

@Accessors(chain = true)
public class User {

//    @TableId(type = IdType.AUTO)

    private Long id;

    private Integer age;

    private String name;

    private String email;
}
