package com.test.mp.mp1111.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.mp.mp1111.entity.User;

public interface UserMapper extends BaseMapper<User> {

}
